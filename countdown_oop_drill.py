class Counter:

    def __init__(self):
        self.value=0
    
    def incr(self):
        self.value+=1

    def decr(self):
        self.value-=1

    def incrby(self,n):
        self.value+=n

    def decrby(self,n):
        self.value-=n    

count=Counter()
print(count.value)

count.incr()
print(count.value)

count.decr()
print(count.value)

count.incrby(4)
print(count.value)

count.decrby(2)
print(count.value)



