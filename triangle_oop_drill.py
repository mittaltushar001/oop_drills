class Triangle:

    def __init__(self):
        
        self.points_list = list()

    def add_point(self,pointx, pointy):
        self.points_list.append((pointx,pointy))

    
    def calc_length(self,x1,y1,x2,y2):
        length = 0.1
        length = float(( abs(x2-x1)**2 + abs(y2-y1)**2 ) ** 0.5)
        return length


    def perimeter(self):
        length1 = self.calc_length(self.points_list[0][0], self.points_list[0][1], self.points_list[1][0], self.points_list[1][1])
        length2 = self.calc_length(self.points_list[1][0], self.points_list[1][1], self.points_list[2][0], self.points_list[2][1])
        length3 = self.calc_length(self.points_list[0][0], self.points_list[0][1], self.points_list[2][0], self.points_list[2][1])

        perimeter_triangle = length1 + length2 + length3

        print(perimeter_triangle)


    def __eq__(self, other_point):
        self.flag = True
        for point in self.points_list:
            if point not in other_point.points_list:
                self.flag = False
        if self.flag:
            print("Both triangles have same points")
        else:
            print("Both triangles have different points")


t1=Triangle()
t1.add_point(0,0)
t1.add_point(0,3)
t1.add_point(4,0)

t1.perimeter()

t2=Triangle()
t2.add_point(1,2)
t2.add_point(2,1)
t2.add_point(1,5)

t2.perimeter()

#t1.is_equal(t2)

t3=Triangle()
t3.add_point(1,2)
t3.add_point(2,1)
t3.add_point(1,5)

t1 == t3

#t1.is_equal(t3)
#t3.is_equal(t1)

t1 == t3
